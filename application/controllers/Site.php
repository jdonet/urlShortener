<?php

class Site extends CI_Controller {

  public function contact() {
    $this->load->helper("form");
    $this->load->library('form_validation');

    $data["title"] = "Contact";

    $this->load->view('common/header', $data);
    if ($this->form_validation->run()) {
      $this->load->library('email');
      $this->email->from($this->input->post('email'), $this->input->post('name'));
      $this->email->to('sebastien.adam.webdev@gmail.com');
      $this->email->subject($this->input->post('title'));
      $this->email->message($this->input->post('message'));
      if ($this->email->send()) {
        $data['result_class'] = "alert-success";
        $data['result_message'] = "Merci de nous avoir envoyé ce mail. Nous y répondrons dans les meilleurs délais.";
      } else {
        $data['result_class'] = "alert-danger";
        $data['result_message'] = "Votre message n'a pas pu être envoyé. Nous mettons tout en oeuvre pour résoudre le problème.";
        $this->email->clear();
      }
      $this->load->view('site/contact_result', $data);
    } else {
      $this->load->view('site/contact', $data);
    }
    $this->load->view('common/footer', $data);
  }

  public function index() {
    $data["title"] = "Mon raccourcisseur url";

    $this->load->view('common/header', $data);
    $this->load->view('site/index', $data);
    $this->load->view('common/footer', $data);
  }

  public function google() {
    $data["title"] = "Raccourcisseur url Google";

    $this->load->view('common/header', $data);
    $this->load->view('site/google', $data);
    $this->load->view('common/footer', $data);
  }

  public function formulaire() {

    //Chargement HTML Helper
    $this->load->helper('html');
    //Chargement Form Helper
    $this->load->helper('form');
    //Chargement url Helper
    $this->load->helper('url');

    $data["title"] = "Raccourcisseur url Perso";

    //librairie elasticSearch
    $this->load->library('elasticSearch');
    //chargement model Url
    $this->load->model('url');
    //récupérer toutes les urls
    $results = $this->elasticsearch->query_all("*:*");
    //ne prendre que les enregistrements qui nous intéressent s'il y en a
    if(isset($results["hits"]["hits"])){

      $results = $results["hits"]["hits"];
      //création tableau d'urls
      $lesURL = array();
      for ($i=0; $i < count($results); $i++) { 
        //création des instances d'url
        if(isset($results[$i]["_source"]["shortenURL"])){
          $u = site_url()."/Site/redirect/".$results[$i]["_source"]["shortenURL"];
        }else{
          $u = "";
        }
        $lesURL[] = new Url($results[$i]["_source"]["url"],$results[$i]["_source"]["dateURL"],$u);
      }
      //ajouts des url aux data à envoyer à la vue
      $data["urls"] = $lesURL; 
    }


    $this->load->view('common/header', $data);
    $this->load->view('site/formulaire', $data);
    $this->load->view('common/footer', $data);
    

  }
  function isURL($url){
    if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
      $this->form_validation->set_message('isURL', 'The URL provided is not valid');
      return FALSE;
    }else{
      return TRUE;
    }    
  }
  public function traitementFormulaire() {
    
    //validation du formulaire
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

    $this->form_validation->set_rules('url', 'Url', 'required');
    $this->form_validation->set_rules('url', 'Url','callback_isURL' );


    if ($this->form_validation->run() == TRUE)
    { // s'il n'y a pas d'erreur de format d'url, on insère
      //Chargement ElasticSearch
      $this->load->library('elasticSearch');
      //Chargement Tools perso
      $this->load->library('tools');
      //préparation des données à insérer dans ElasticSearch
      $data = array("url"=>$this->input->post('url'), "dateURL"=>date('Y-m-d'),"shortenURL"=>$this->tools->generateCode());
      //récupération du nombre d'enregistrements, 0 s'il n'y en a pas
      $id = $this->elasticsearch->count("url")["count"] ?? 0;
      //incrémentation
      $id +=1;
      //ajout du nouvel enregistrement
      $this->elasticsearch->add("url", $id, $data);
      //attendre 1 seconde avant de recharger l'interface
      sleep(1);
      //chargement interface
    }
    //Dans tous les cas, on ré affiche le formulaire
    $this->formulaire();
  }

public function redirect($code) {
  //librairie elasticSearch
  $this->load->library('elasticSearch');
  //recherche de l'url dont le code est passé en paramètre
  $url = $this->elasticsearch->advancedquery("url",
  '
  {
    "query":{
      "query_string": {
        "query":"'.$code.'",
        "fields":[
          "shortenURL"
        ]
      }
    }
  }
  '
  );
  //récupération uniquement de l'url
  $data["url"] = $url["hits"]["hits"][0]["_source"]["url"];
  //appel de la vue en lui passant l'url
  $this->load->view('site/redirect', $data);
}
  
}