<?php

class Url extends CI_Model {

    private $pathUrl;
    private $dateUrl;
    private $shortenUrl;
    private $author;

    public function __construct(String $pathUrl = null, String $dateUrl = null, String $shortenUrl = null, String $author=null) {
        parent::__construct();
        $this->pathUrl = $pathUrl;
        $this->dateUrl = $dateUrl;
        $this->shortenUrl = $shortenUrl;
        $this->author = $author;
    }


    public function generateShortenUrl()
    {
        
    }
    public function getPathUrl(){
        return $this->pathUrl;
    }
    public function getDateUrl(){
        return $this->dateUrl;
    }
    public function getshortenUrl(){
        return $this->shortenUrl;
    }
}