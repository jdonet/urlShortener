<div class="jumbotron">
  <div class="container">
    <?= heading($title); ?>
  </div>
</div>
<div class="alert alert-danger" role="alert">
  <?= validation_errors(); //affichage des erreurs de validation du formulaire ?>
</div>
<div class="container">
  <div class="row">
    <?= form_open('traitementFormulaire', ['class' => 'form-horizontal']); ?>
    <div class="form-group <?= empty(form_error('name')) ? '' : 'has-error' ?>">
      <?= form_label("Votre URL&nbsp;:", "url", ['class' => "col-md-2 control-label"]) ?>
      <div class="col-md-10">
        <?= form_input(['name' => "url", 'id' => "url", 'class' => 'form-control'], set_value('name')) ?>
        <span class="help-block"><?= form_error('name'); ?></span>
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-offset-2 col-md-10">
        <?= form_submit("send", "Raccourcir mon URL", ['class' => "btn btn-default"]); ?>
      </div>
    </div>
    <?= form_close() ?>
  </div>
  <?= heading("URL insérées"); ?>
  <table class="table">
  <thead>
    <tr>
      <th>Date</th>
      <th>URL longue</th>
      <th>URL courte</th>
    </tr>
  </thead>
  <tbody>
    <?php 
    if(isset($urls)){
      for ($i=0; $i < count($urls); $i++) { 
      ?>
      <tr>
        <td><?= $urls[$i]->getDateUrl() ?></td>
        <td><?= $urls[$i]->getPathUrl() ?></td>
        <td><a href="<?= $urls[$i]->getshortenUrl() ?>"><?= $urls[$i]->getshortenUrl() ?></a></td>
      </tr>
      <?php 
      }
    }
    ?>
  </tbody>
</table>

</div>